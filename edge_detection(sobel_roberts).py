import matplotlib.pyplot as plt

from skimage.data import camera
from skimage.filters import roberts, sobel, scharr, prewitt

image = camera()
edge_roberts = roberts(image)
edge_sobel = sobel(image)
fig, ax = plt.subplots(ncols=3, sharex=True, sharey=True, figsize=(8, 4))
ax[0].imshow(image, cmap=plt.cm.gray)
ax[1].imshow(edge_roberts, cmap=plt.cm.gray)
ax[2].imshow(edge_sobel, cmap=plt.cm.gray)
plt.tight_layout()
plt.show()
